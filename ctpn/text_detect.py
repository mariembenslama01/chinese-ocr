from ctpn.model import ctpn
from ctpn.detectors import TextDetector
from ctpn.other import draw_boxes
import numpy as np
import cv2
def text_detect(img):
    scores, boxes,img = ctpn(img)
    #draw_m(img,boxes,'1.jpg') many dense boxes
    textdetector  = TextDetector()
    boxes = textdetector.detect(boxes,scores[:, np.newaxis],img.shape[:2])
    #print type(boxes),boxes.shape
    #print boxes
    #draw_m(img,boxes,'2.jpg') horizontal boxex
    text_recs,tmp = draw_boxes(img, boxes, caption='im_name', wait=True,is_display=False)
    #print type(text_recs),text_recs.shape
    #print text_recs
    #cv2.imwrite('3.jpg',tmp) tmp is boxes with little angles
    return text_recs,tmp,img

def draw_m(img,boxes,fn): #only for test by weibin
    
    im = img.copy()
    for i in range(0,len(boxes)):
        box = boxes[i]
        cv2.rectangle(im,(box[0],box[1]),(box[2],box[3]),(0,255,0),1)
    cv2.imwrite(fn,im)
    
    
    return